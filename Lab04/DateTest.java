package com.oolt.labs;

public class DateTest {
	public static void main(String[] args) {
		MyDate date1 = new MyDate();
		date1.print(); // Thoi gian: 17/4/2022
		
		MyDate date2 = new MyDate(12, 5, 2017);
		date2.print(); // Thoi gian: 12/5/2017
		
		MyDate date3 = new MyDate("February 18th 2019");
		date3.print(); // Thoi gian: 18/2/2019
		
		date1.accept(); // Feb 3 2022
		date1.print(); // Thoi gian: 3/2/2022
	}
}
