package com.oolt.labs;

import java.util.List;

public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	public static final int MAX_LIMITTED_ORDERS = 5;
	private static int nbOrders = 0;
	private int qtyOrdered = 0;
	
	
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	private MyDate dateOrdered = new MyDate();	
	
	
	
	Order() {
		if (nbOrders >= MAX_LIMITTED_ORDERS) {
			return;
		}
		nbOrders++;
	}
	
	public boolean addDigitalVideoDisc(DigitalVideoDisc disc) {
		if (qtyOrdered < MAX_NUMBERS_ORDERED) {
			itemsOrdered[qtyOrdered++] = disc;
			System.out.println("The disc has been added");
			return true;
		} else {
			System.out.println("The order is almost full");
			return false;
		}
	}

	public boolean addDigitalVideoDisc(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2) {
		if (qtyOrdered + 2 > MAX_NUMBERS_ORDERED) {
			System.out.println("The order is almost full");
			return false;
		}
		itemsOrdered[qtyOrdered++] = dvd1;
		itemsOrdered[qtyOrdered++] = dvd2;
		return true;
	}
	
	public boolean addDigitalVideoDisc(DigitalVideoDisc [] dvdList) {
		if (dvdList.length + qtyOrdered > MAX_NUMBERS_ORDERED) {
			System.out.println("Not enough space to add dvdList");
			return false;			
		}
		
		for (DigitalVideoDisc e : dvdList) {
			addDigitalVideoDisc(e);
		}
		return true;
	}
	
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		for (int i=0; i<qtyOrdered; i++) {
			if (disc == itemsOrdered[i]) {
				itemsOrdered[i] = itemsOrdered[qtyOrdered - 1];
				qtyOrdered--;
				System.out.println("The disc has been removed");
				return;
			}
			System.out.println("The disc not exist in order list");
		}
	}
	
	public float totalCost() {
		float ans = 0;
		for (int i=0; i<qtyOrdered; i++) {
			ans+= itemsOrdered[i].getCost();
		}
		return ans;
	}
	
	public void printInfor() {
		System.out.println("***********************Order***********************");
		System.out.print("Date: ");
		this.dateOrdered.print("dd-MMM-yyyy");
		System.out.println("Ordered Items:");
		for (int i=0; i<qtyOrdered; i++) {
			System.out.printf("%d. DVD - %s - %s - %s - %d: %s $\n", 
					i + 1,
					itemsOrdered[i].getTitle(),
					itemsOrdered[i].getCategory(),
					itemsOrdered[i].getDirector(),
					itemsOrdered[i].getLength(),
					itemsOrdered[i].getCost()
				);
		}
		System.out.printf("Total Cost: %f $\n",totalCost());
		System.out.println("***************************************************");
	}
}
