package com.oolt.labs;

public class DigitalVideoDisc {
	private String title;
	private String category;
	private String director;
	private int length;
	private float cost;
	
	// Constructor
	public DigitalVideoDisc() {
		this.category = "";
		this.title = "";
		this.director = "";
		this.length = 5;
		this.cost = 0.5f;
	}
	/**
	 * @param title
	 */
	public DigitalVideoDisc(String title) {
		this.title = title;
	}
	/**
	 * @param title
	 * @param category
	 */
	public DigitalVideoDisc(String title, String category) {
		this.title = title;
		this.category = category;
	}
	/**
	 * @param title
	 * @param category
	 * @param director
	 * @param length
	 * @param cost
	 */
	public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
		this.title = title;
		this.category = category;
		this.director = director;
		this.length = length;
		this.cost = cost;
	}
	
	// Cac phuong thuc
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = Math.max(0, length);
	}
	public float getCost() {
		return cost;
	}
	public void setCost(float cost) {
		this.cost = Math.max(0, cost);
	}
	public void printInfor() {
		System.out.printf("%s\n%s\n%s\n%d\n%f\n", title, category, director, length, cost);
	}
}
