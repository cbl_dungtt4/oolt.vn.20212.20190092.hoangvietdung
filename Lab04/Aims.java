package com.oolt.labs;

import java.util.ArrayList;
import java.util.List;

public class Aims {

	public static void main(String[] args) {
		Order anOrder = new Order();
		DigitalVideoDisc dvd1 = new 
				DigitalVideoDisc (
					"The Lion King",
					"Animation",
					"Roger Allers",
					87,
					19.95f
				);
		DigitalVideoDisc dvd2 = new
				DigitalVideoDisc(
					"Star war", 
					"Science fiction",
					"George lucas", 
					124, 
					24.95f
				);
		DigitalVideoDisc dvd3 = new
				DigitalVideoDisc(
					"Aladdin",
					"Animation",
					"John Musker",
					90,
					18.99f
				);
		DigitalVideoDisc dvd4 = new
				DigitalVideoDisc(
					"Doraemon",
					"Anime",
					"Nasakajun",
					94,
					12.39f
				);
		DigitalVideoDisc dvd5 = new
				DigitalVideoDisc(
					"1001 con chó đốm",
					"Hoạt hình",
					"Su shi nano",
					121,
					28.99f
				);
		// dvd1.printInfor();
		anOrder.addDigitalVideoDisc(dvd3);
		anOrder.addDigitalVideoDisc(dvd2);
		anOrder.addDigitalVideoDisc(dvd1);
		
		DigitalVideoDisc[] dvdList = new DigitalVideoDisc[3];
		dvdList[0] = dvd2;
		dvdList[1] = dvd1;
		dvdList[2] = dvd3;
		
		anOrder.addDigitalVideoDisc(dvdList);
		anOrder.addDigitalVideoDisc(dvdList);
		anOrder.addDigitalVideoDisc(dvdList);
		anOrder.addDigitalVideoDisc(dvdList);
		
		
		Order order2 = new Order();
		order2.addDigitalVideoDisc(dvdList);
		order2.addDigitalVideoDisc(dvd2);
		
		order2.printInfor();
	}

}
