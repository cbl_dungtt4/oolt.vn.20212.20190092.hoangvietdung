package com.oolt.labs;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import date.MyDate;
import media.Book;
import media.Media;

public class Order {
	private static final int MAX_NUMBERS_ORDERED = 10;
	private static final int MAX_LIMITTED_ORDERS = 5;
	private ArrayList<Media> itemsOrdered = new ArrayList<Media>();
	private static int nbOrders = 0;
	private MyDate dateOrdered = new MyDate();	
	
	
	
	public Order() {
		if (nbOrders >= MAX_LIMITTED_ORDERS) {
			return;
		}
		nbOrders++;
	}
	public void addMedia() 
	{
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("Nhap title: ");
		String title = keyboard.nextLine();
		System.out.print("Nhap category: ");
		String category = keyboard.nextLine();
		System.out.print("Nhap cost: ");
		float cost = keyboard.nextFloat();
		keyboard.nextLine();
		System.out.println("Nhap authors: ");
		
		Book book = new Book(title, category, cost, new ArrayList<String>());
		
		for (int i=1; ; i++) {
			System.out.printf("Author %d (Nếu không còn tác giả, để trống và nhấn Enter): ", i);
			String author = keyboard.nextLine();
			if (author.length() == 0) {
				break;
			}
			book.addAuthor(author);
		}
		this.addMedia(book);
		System.out.println("The media has been added");
	 }
	public void addMedia(Media media) {
		if (!itemsOrdered.contains(media)) {
			itemsOrdered.add(media);
		}
	}
	public void removeMedia(Media media) {
		if (itemsOrdered.contains(media)) {
			itemsOrdered.remove(media);
		}
	}
	public void removeMedia(int stt) {
		if (itemsOrdered.size() < stt) {
			System.out.println("The number you entered is larger than order's size");
		} else {
			itemsOrdered.remove(stt+1);
		}
	}
	
//	public Media getALuckyItem() {
//		Random rd = new Random();
//		int luckyNumber = rd.nextInt(itemsOrdered.size());
//		itemsOrdered.get(luckyNumber).setCost(0);
//		return itemsOrdered.get(luckyNumber);
//	}
	
	public float totalCost() {
		float ans = 0;
		for (Media item : itemsOrdered) {
			ans+= item.getCost();
		}
		return ans;
	}
	
	public void printInfor() {
		System.out.println("***********************Order***********************");
		System.out.print("Date: ");
		this.dateOrdered.print("dd-MMM-yyyy");
		System.out.println("Ordered Items:");
		for (int i=0; i<itemsOrdered.size(); i++) {
			System.out.printf("%d. Media - %s - %s: %f $\n", 
					i+1,
					itemsOrdered.get(i).getTitle(),
					itemsOrdered.get(i).getCategory(),
//					itemsOrdered.get(i).getDirector(),
//					itemsOrdered.get(i).getLength(),
					itemsOrdered.get(i).getCost()
				);
		}
		System.out.printf("Total Cost: %f $\n",totalCost());
		System.out.println("***************************************************");
	}
}
