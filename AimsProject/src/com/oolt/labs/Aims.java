package com.oolt.labs;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import media.Book;
import media.CompactDisc;
import media.DigitalVideoDisc;
import media.Media;

public class Aims {
	public static void showMenu() {
		Order order = new Order();
		do {
			System.out.println("--------Order Management Application--------");
			System.out.println("\t1. Create new order");
			System.out.println("\t2. Add item to the order");
			System.out.println("\t3. Delete item");
			System.out.println("\t4. Display the item list of order");
			System.out.println("\t0. Exit");
			System.out.println("------------------------------");
			System.out.println("Please choose a number: 0-1-2-3-4");
			
			Scanner sc = new Scanner(System.in);
			int temp = sc.nextInt();
			switch (temp) {
			case 1:
				order = new Order();
				break;
			case 2:
				System.out.println("Choose type of media:");
				System.out.println("  1. Book");
				System.out.println("  2. Compact disc");
				System.out.println("  3. Digital video disc");
				
				int t = sc.nextInt();
				if (t == 1) {
					order.addMedia(new Book());
				}
				else if (t == 2) {
					order.addMedia(new CompactDisc());
				}
				else if (t == 3) {
					order.addMedia(new DigitalVideoDisc());
				}
				else {
					System.out.println("Invalid command");
				}
				break;
			case 3:
				order.printInfor();
				System.out.println("\tEnter number to delete");
				int stt = sc.nextInt();
				order.removeMedia(stt);
				break;
			case 4:
				order.printInfor();
				break;
			case 0: 
				return;
			default: 
				System.out.println("Invalid number!");
				break;
			}
		} while (true);
	}

	public static void main(String[] args) {
		MemoryDaemon md = new MemoryDaemon();
		Thread th = new Thread(md);
		th.setDaemon(true);
		th.start();
		
		showMenu();
	}

}
