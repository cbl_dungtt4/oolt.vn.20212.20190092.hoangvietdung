package com.test;

import java.util.Random;

public class ConcatenationInLoops {
	public static void main(String args[]) {
		Random r = new Random(103);
		long start = System.currentTimeMillis();
		String s = "";
		for (int i = 0; i < 65536; i++) {
			s+= r.nextInt(2);
		}
		System.out.println(s.charAt(5));
		System.out.println(System.currentTimeMillis() - start);
		
		r = new Random(103);
		long start2 = System.currentTimeMillis();
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 65536; i++) {
			sb.append(r.nextInt(2));
		}
		System.out.println(sb.charAt(5));
		System.out.println(System.currentTimeMillis() - start2);
	}
}
