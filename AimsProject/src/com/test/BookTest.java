package com.test;

import media.Book;

public class BookTest {
	public static void main(String[] args) {
		Book book = new Book("De men", "Ke ve hanh trinh cua de", 123);
		book.setContent("Dế Mèn phiêu lưu ký được xem là những trang văn mẫu mực của văn học thiếu nhi. Dường như mọi câu, đoạn, hình ảnh đều tác động mạnh mẽ đến tư tưởng tình cảm thẩm mỹ của người đọc. Tác phẩm miêu tả cuộc phiêu lưu của một chú Dế Mèn qua thế giới loài vật và loài người.");
		
		System.out.println(book.toString());
	}
}
