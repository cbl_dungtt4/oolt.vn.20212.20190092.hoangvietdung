package com.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import media.*;

public class TestMediaCompareTo {
	public static void main(String[] args) {
		List<Media> collection = new ArrayList<Media>();
		System.out.println("aaaaa");
		collection.add(new Book("Book1", "", 12.3f));
		System.out.println("aaaaa");
		collection.add(new Book("Book2", "", 22.1f));
		System.out.println("aaaaa");
		collection.add(new DigitalVideoDisc("Axinc", "as", 22.3f));
		System.out.println("aaaaa");
		collection.add(new CompactDisc("a,CD 01", "", 21.0f, 10, "VietDung"));
		System.out.println("aaaaa");
		collection.add(new CompactDisc("A.xinch", "catego", 22f, 104, "Toan"));
		
		System.out.println("------The Medias currently in the order are:------");
		Iterator it = collection.iterator();
		while (it.hasNext()) {
			System.out.println(  ((Media) it.next()).getTitle()  );
		}
		
		Collections.sort((ArrayList) collection);
		
		System.out.println("------The Medias in sorted order are:------");
		it = collection.iterator();
		while (it.hasNext()) {
			System.out.println(  ((Media) it.next()).getTitle()  );
		}
	}
}
