package com.test;

import com.oolt.labs.Order;

import media.DigitalVideoDisc;

public class DiskTest {
	public static void main(String argsp[]) {
		Order anOrder = new Order();
		DigitalVideoDisc dvd1 = new 
				DigitalVideoDisc (
					"The Lion King",
					"Animation",
					"Roger Allers",
					87,
					19.95f
				);
		DigitalVideoDisc dvd2 = new
				DigitalVideoDisc(
					"Star war", 
					"Science fiction",
					"George lucas", 
					124, 
					24.95f
				);
		DigitalVideoDisc dvd3 = new
				DigitalVideoDisc(
					"Aladdin",
					"Animation",
					"John Musker",
					90,
					18.99f
				);
		DigitalVideoDisc dvd4 = new
				DigitalVideoDisc(
					"Doraemon",
					"Anime",
					"Nasakajun",
					94,
					12.39f
				);
		DigitalVideoDisc dvd5 = new
				DigitalVideoDisc(
					"1001 con chó đốm",
					"Hoạt hình",
					"Su shi nano",
					121,
					28.99f
				);
		// dvd1.printInfor();
	}
}
