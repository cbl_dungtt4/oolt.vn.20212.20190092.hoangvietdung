package media;

import java.util.Scanner;

public class Disc extends Media {
	protected float length;
	protected String director;
	
	public Disc() {
		super();
		Scanner keyboard = new Scanner(System.in);
		System.out.print("Enter length: ");
		this.length = keyboard.nextFloat();
		keyboard.nextLine();
		System.out.print("Enter director: ");
		this.director = keyboard.nextLine();
	}
	public Disc(String title, String category, float cost) {
		super(title, category, cost);
	}
	public Disc(String title, String category, float cost, int length, String director) {
		this(title, category, cost);
		this.length = length;
		this.director = director;
	}
	
	public float getLength() {
		return length;
	}
	public String getDirector() {
		return director;
	}	
}
