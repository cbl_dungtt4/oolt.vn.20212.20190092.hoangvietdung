package media;

import java.util.ArrayList;
import java.util.Scanner;

public class CompactDisc extends Disc implements Playable {
	private String artist;
	private ArrayList<Track> tracks;
	
	public CompactDisc() {
		super();
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("Enter artist: ");
		this.artist = keyboard.nextLine();
		
		System.out.println("Enter tracks: ");
		ArrayList<Track> tracks = new ArrayList<Track>();
		do {
			System.out.println("Is(are) there still track(s)?");
			System.out.println("\t1: YES\n\t0: NO");
			int t = keyboard.nextInt();
			if (t == 1) {
				tracks.add(new Track());				
			} else {
				break;
			}
		} while (true);
	}
	public CompactDisc(String title, String category, float cost, int length, String director) {
		super(title, category, cost, length, director);
	}
	public CompactDisc(String title, String category, 
			float cost, int length, String director, String artist, ArrayList<Track> tracks) {
		super(title, category, cost, length, director);
		this.artist = artist;
		this.tracks = tracks;
	}
	
	
	public String getArtist() {
		return this.artist;
	}
	
	public void addTrack(Track track) {
		if (!this.tracks.contains(track)) {
			this.tracks.add(track);
		} else {
			System.out.println("This track already exist");
		}
	}
	
	public void removeTrack(Track track) {
		if (this.tracks.contains(track)) {
			this.tracks.remove(track);
		} else {
			System.out.println("This track is not exist");
		}
	}
	
	public float getLength() {
		float sum = 0;
		for (Track track : tracks) {
			sum+= track.getLength();
		}
		return sum;
	}
	@Override
	public void play() {
		for (Track track : tracks) {
			track.play();
		}
	}
	public int compareTo(CompactDisc cd) {

		if (this.tracks.size() < cd.tracks.size()) {
			return -1;
		}
		else if (this.tracks.size() > cd.tracks.size()) {
			return 1;
		}
		else {
			if (this.getLength() < cd.getLength()) return -1;
			else if (this.getLength() < cd.getLength()) return 1;
			else return 0;
		}
	}
}
