package media;

import java.util.ArrayList;
import java.util.Scanner;

public abstract class Media implements Comparable<Media> {
	protected int id;
	protected String title;
	protected String category;
	protected float cost;
	
	public Media() 
	{
		Scanner keyboard = new Scanner(System.in);
		System.out.print("Enter title: ");
		this.title = keyboard.nextLine();
		System.out.print("Enter category: ");
		this.category = keyboard.nextLine();
		System.out.print("Enter cost: ");
		this.cost = keyboard.nextFloat();
		keyboard.nextLine();
	}
	public Media(String title) {
		this.title = title;
	}
	public Media(String title, String category, float cost) {
		this(title);
		this.category = category;
		this.cost = cost;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return this.id;
	}
	public String getTitle() {
		return title;
	}

	public String getCategory() {
		return category;
	}

	public float getCost() {
		return cost;
	}

	
	public boolean equals(Object o) {
		Media m = (Media) o;
		return this.id == m.getId();
	}
	
	public int compareTo(Media o) {
		return this.title.compareTo(((Media) o).getTitle());
	}
}
