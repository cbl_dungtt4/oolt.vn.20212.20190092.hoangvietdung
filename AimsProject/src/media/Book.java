package media;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Book extends Media {
	private List<String> authors = new ArrayList<String>();
	private String content;
	private List<String> contentTokens;
	private Map<String, Integer> wordFrequency;
	
	public Book() {
		super();
		
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Enter authors: ");
		List<String> authors = new ArrayList<String>();
		
		for (int i=1; ; i++) {
			System.out.printf("Author %d (Nếu không còn tác giả, để trống và nhấn Enter): ", i);
			String author = keyboard.nextLine();
			if (author.length() == 0) {
				break;
			}
			authors.add(author);
		}
		this.authors = authors;
	}
	
	public Book(String title) {
		super(title);
	}
	public Book(String title, String category, float cost) {
		super(title, category, cost);
	}
	public Book(String title, String category, float cost, List<String> authors) {
		super(title, category, cost);
		this.authors = authors;
	}

	public List<String> getAuthors() {
		return authors;
	}

	public void setContent(String content) {
		this.content = content;
		this.processContent();
	}
	public String getContent() {
		return this.content;
	}
	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}
	public void addAuthor(String authorName) {
		if (!(this.authors.contains(authorName))) {
			this.authors.add(authorName);
		}
	}
	public void removeAuthor(String authorName) {
		if (this.authors.contains(authorName)) {
			this.authors.remove(authorName);
		}
	}
	public void processContent() {
		// split by punctuations
		contentTokens = new ArrayList<String>();
		String[] temp =  content.split("[\\p{Punct}\\s]+");
		Arrays.sort(temp);
		contentTokens.addAll(Arrays.asList(temp));
		
		// Count the frequency of each token
		wordFrequency = new TreeMap<String, Integer>();
		String pre = "";
		int count = 0;
		for (String token : contentTokens) {
			if (token.compareTo(pre) != 0) {
				if (count > 0) {
					wordFrequency.put(pre, count);
				}
			} else {
				count++;
			}
			pre = token;
		}
		if (count > 0) {
			wordFrequency.put(pre, count);
		}
	}
	
	public String toString() {
		String res = "Title: " + title + "\n"
				+ "Category: " + category + "\n"
				+ "Cost: " + cost + "\n"
				+ "Authors:";
		for (String author:authors) {
			res+= " " + author;
		}
		res+= "\nContent length: " + contentTokens.size() + "\n";
		
		for (Map.Entry<String, Integer> word : wordFrequency.entrySet()) {
			res+= word.getKey() + "-" + word.getValue() + "\n";
		}
		return res;
	}
}
