package media;

public class DigitalVideoDisc extends Disc implements Playable {
	// Constructor
	public DigitalVideoDisc() {
	}
	/**
	 * @param title
	 */
	public DigitalVideoDisc(String title) {
		this.title = title;
	}
	/**
	 * @param title
	 * @param category
	 */
	public DigitalVideoDisc(String title, String category, float cost) {
		super(title, category, cost);
	}
	/**
	 * @param title
	 * @param category
	 * @param director
	 * @param length
	 * @param cost
	 */
	public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
		this.title = title;
		this.category = category;
		this.director = director;
		this.length = length;
		this.cost = cost;
	}
	
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public float getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = Math.max(0, length);
	}
	public void printInfor() {
		System.out.printf("%s\n%s\n%s\n%d\n%f\n", title, category, director, length, cost);
	}
	public boolean search(String title) {
		String[] arrTitle = this.title.split(" ");    
		String[] arrString = title.split(" ");

		for ( String a1 : arrString) {
			boolean found = false;
		    for ( String a2 : arrTitle) {
		    	if (a1.equalsIgnoreCase(a2)) {
		    		found = true;
		    		break;
		    	}
		    }
		    if (!found) {
		    	return false;
		    }
		}
		return true;
	}
	@Override
	public void play() {
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
	}
	public int compareTo(DigitalVideoDisc dvd) {
		if (this.cost < (dvd).cost) return -1;
		else if (this.cost > (dvd).cost) return 1;
		return 0;
	}
}
