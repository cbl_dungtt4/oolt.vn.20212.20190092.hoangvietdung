package media;

import java.util.Scanner;

public class Track implements Playable {
	private String title;
	private float length;
	
	public Track() {
		Scanner keyboard = new Scanner(System.in);
		System.out.print("Enter title: ");
		this.title = keyboard.nextLine();
		System.out.print("Enter length: ");
		this.length = keyboard.nextFloat();
	}
	public Track(String title, int length) {
		this.title = title;
		this.length = length;
	}
	public String getTitle() {
		return title;
	}
	public float getLength() {
		return length;
	}
	@Override
	public void play() {
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
	}
	public boolean equals(Object o) {
		Track t = (Track) o;
		return (t.getLength() == this.length) && (this.title.equals(t));
	}
}
