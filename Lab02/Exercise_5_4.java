package thucHanh.Lab02;

import java.util.Scanner;

public class Exercise_5_4 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter height of triangle");
		
		int n = sc.nextInt();
		
		for (int i=0; i<n; i++) {
			String str = "";
			for (int j=1; j <= 2*n - 1; j++) {
				if (n-i <= j && j <= n+i) {
					str+= '*';
				} else {
					str+= ' ';
				}
			}
			System.out.println(str);
		}
	}
}
