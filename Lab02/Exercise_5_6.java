package thucHanh.Lab02;

import java.util.Arrays;
import java.util.Scanner;

public class Exercise_5_6 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int n = sc.nextInt();
		int[] a = new int[n];
		for (int i=0; i<n; i++) {
			a[i] = sc.nextInt();
		}
		
		Arrays.sort(a);
		System.out.printf("Sorted array: %s\n", Arrays.toString(a));
		
		int sum = 0;
		for (int i=0; i<n; i++) {
			sum+= a[i];
		}
		System.out.printf("Sum: %d\nAverage: %f", sum, (double)sum/n);
	}
}
