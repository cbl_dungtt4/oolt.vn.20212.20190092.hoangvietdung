package thucHanh.Lab02;

public class Exercise_5_7 {
	public static void main(String[] args) {
		int[][] a = {
				{3, 5, 8, 9, 5},
				{2, -1, 6, 9, 8},
				{0, 3, 1, -6, 6}
		};
		int[][] b = {
				{6, 4, 1, 8, -5},
				{2, 4, -7, 0, 8},
				{-1, 5, 3, 0, 1}
		};

		System.out.println("A =");
		for (int i=0; i<3; i++) {
			for (int j=0; j<5; j++) {
				System.out.printf("%3d", a[i][j]);
			}
			System.out.println();
		}
		
		System.out.println("B =");
		for (int i=0; i<3; i++) {
			for (int j=0; j<5; j++) {
				System.out.printf("%3d", b[i][j]);
			}
			System.out.println();
		}
		
		System.out.println("A + B =");
		for (int i=0; i<3; i++) {
			for (int j=0; j<5; j++) {
				System.out.printf("%3d", a[i][j] + b[i][j]);
			}
			System.out.println();
		}
	}
}
