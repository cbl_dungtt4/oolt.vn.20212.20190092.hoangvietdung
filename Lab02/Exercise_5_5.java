package thucHanh.Lab02;

import java.util.Scanner;

import javax.swing.JOptionPane;

public class Exercise_5_5 {
	static boolean isLeapYear(int year) {
		if (year % 4 == 0 && !(year % 100 == 0 && year % 400 != 0)) {
			return true;
		}
		return false;
	}
	static String[] months = {
			"January", "February", "March", "April", "May", "June",
			"July", "August", "September", "October", "November", "December",
			"Jan.", "Feb.", "Mar.", "Apr.", "May", "June",
			"July", "Aug.", "Sept.", "Oct.", "Nov.", "Dec.",
			"Jan", "Feb", "Mar", "Apr", "May", "Jun",
			"Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
			"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"};
	static int[] days = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	
	public static void main(String[] args) {
		Scanner Sc = new Scanner(System.in);
		
		do {
			System.out.print("Nhập tháng: ");
			String strMonth = Sc.nextLine();
			
			System.out.print("Nhập năm: ");
			String strYear = Sc.nextLine();
			
			int thang = 0;
			
			for (int i=0; i<months.length; i++) {
				if (strMonth.equals(months[i])) {
					thang = i % 12 + 1;
					break;
				}
			}
			
			if (thang == 0) {
				int option = JOptionPane.showConfirmDialog(null, 
						"Sai định dạng tháng, bạn có muốn nhập lại?");
				if (option == JOptionPane.YES_OPTION) {
					continue;
				} else {
					System.exit(0);
				}
			}
			
			int nam = Integer.parseInt(strYear);
			int ngay = days[thang - 1];
			
			if (isLeapYear(nam) && thang == 2) {
				ngay = 29;
			}
			
			String message = String.format("Thang %d/%d co %d ngay", thang, nam, ngay);
			
			System.out.println(message);
			break;
		} while (true);
		
		
	}
}
