package thucHanh.Lab01;

import javax.swing.*;

public class SolveEquation {
	
	
	
	static void solveFirstDegreeEquation() {
		JPanel myPanel = new JPanel();
		JTextField aField = new JTextField(4);
        JTextField bField = new JTextField(4);
        myPanel.add(aField);
        myPanel.add(new JLabel("x + "));
        myPanel.add(bField);
        myPanel.add(new JLabel(" = 0"));

        int result = JOptionPane.showConfirmDialog(null, myPanel, 
                 "Giải phương trình bậc nhất 1 ẩn", JOptionPane.OK_CANCEL_OPTION);
        
        if (result == JOptionPane.OK_OPTION) {
        	double a = Double.parseDouble(aField.getText());
        	double b = Double.parseDouble(bField.getText());
        	String message;
    		if (a == 0) {
    			if (b == 0) {
    				message = "Phương trình vô số nghiệm";
    			} else {
    				message = "Phương trình vô nghiệm";
    			}
    		} else {
    			message = "x = " + (-b/a);
    		}
    		JOptionPane.showMessageDialog(null, message,
    	            "Kết quả", JOptionPane.INFORMATION_MESSAGE
    	        );
        }
	}
	
	
	
	
	
	
	static void solveFirstDegreeEquationTwoVariables() {
		JPanel line1 = new JPanel();
    	JPanel line2 = new JPanel();
    	JTextField aField = new JTextField(4);
        JTextField bField = new JTextField(4);
        JTextField cField = new JTextField(4);
        JTextField dField = new JTextField(4);
        JTextField eField = new JTextField(4);
        JTextField fField = new JTextField(4);
        
    	line1.add(aField);
    	line1.add(new JLabel("x + "));
    	line1.add(bField);
    	line1.add(new JLabel("y = "));
    	line1.add(cField);
    	
    	line2.add(dField);
    	line2.add(new JLabel("x + "));
    	line2.add(eField);
    	line2.add(new JLabel("y = "));
    	line2.add(fField);
    	
    	Object[] myPanel = {
    		line1, line2
    	};
        int result = JOptionPane.showConfirmDialog(null, myPanel, 
        		"Giải hệ phương trình bậc nhất 2 ẩn", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
        	String message;
        	double a11 = Double.parseDouble(aField.getText());
        	double a12 = Double.parseDouble(bField.getText());
        	double b1 = Double.parseDouble(cField.getText());
        	double a21 = Double.parseDouble(dField.getText());
        	double a22 = Double.parseDouble(eField.getText());
        	double b2 = Double.parseDouble(fField.getText());
        	
        	double D = a11*a22 - a21*a12;
        	double D1 = b1*a22 - b2*a12;
        	double D2 = a11*b2 - a21*b1;
        	
        	if (D != 0) {
        		message = "(x₁, x₂) = (" + D1/D + ", " + D2/D + ")";
        	} else {
        		if (D1 == D2 && D2 == D && D == 0) {
        			message = "Phương trình vô số nghiệm";
        		} else {
        			message = "Phương trình vô nghiệm";
        		}
        	}
        	
        	JOptionPane.showMessageDialog(null, message,
    	            "Kết quả", JOptionPane.INFORMATION_MESSAGE
    	        );
        }
	}
	
	
	
	
	
	
	
	
	
	static void solveSecondDegreeEquation() {
		JTextField aField = new JTextField(4);
        JTextField bField = new JTextField(4);
        JTextField cField = new JTextField(4);
		JPanel myPanel = new JPanel();
        myPanel.add(aField);
        myPanel.add(new JLabel("x\u00b2 + "));
        myPanel.add(bField);
        myPanel.add(new JLabel("x + "));
        myPanel.add(cField);
        myPanel.add(new JLabel(" = 0"));
        
        int result = JOptionPane.showConfirmDialog(null, myPanel, 
        		"Giải phương trình bậc hai 1 ẩn", JOptionPane.OK_CANCEL_OPTION);
        
        if (result == JOptionPane.OK_OPTION) {
        	String message;
        	double a = Double.parseDouble(aField.getText());
        	double b = Double.parseDouble(bField.getText());
        	double c = Double.parseDouble(cField.getText());
        	
        	double delta = b*b - 4*a*c;
        	if (delta == 0) {
        		message = "x = " + (-b/(2*a));
        	} else if (delta > 0) {
        		message = "x₁ = " + ( (-b + Math.sqrt(delta))/(2*a) ) + "\n"
        				+ "x₂ = " + ( (-b - Math.sqrt(delta))/(2*a) );
        	} else {
        		message = "Phương trình vô nghiệm";
        	}
        	
        	JOptionPane.showMessageDialog(null, message,
    	            "Kết quả", JOptionPane.INFORMATION_MESSAGE
    	        );
        }
	}
	
	
	
	
	
	
	
	
	public static void main(String args[]) {
        String[] options = {"Bậc nhất 1 ẩn", "Bậc nhất 2 ẩn", "Bậc hai 1 ẩn"};

        int x = JOptionPane.showOptionDialog(
	    			null,
	    			"Chọn loại phương trình bạn muốn giải",
	    			"Click a button",
	    			JOptionPane.DEFAULT_OPTION,
	    			JOptionPane.INFORMATION_MESSAGE,
	    			null,
	    			options,
	    			options[0]
	    		);
        
        if (x == 0) {
        	solveFirstDegreeEquation();
        } else if (x == 1) {
        	solveFirstDegreeEquationTwoVariables();
        } else if (x == 2) {
        	solveSecondDegreeEquation();
        }
        System.exit(0);
    }
}